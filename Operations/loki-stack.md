Dans cet exercice, vous allez installer la stack Loki en tant que chart Helm. 

:fire: Il y a pour le moment une issue sur GitHub concernant le déploiement de cette stack sur un cluster 1.22, vous ne pouvez donc pas faire cet exercice pour le moment si vous êtes sur un cluster 1.22+.

## Prérequis

Installation du client Helm 3 (https://gitlab.com/lucj/k8s-exercices/-/blob/master/Helm/helm-client.md)

## Loki Stack

La stack Loki contient différents composants permettant de collecter et de visualiser des données de monitoring et des logs.

La documentation du chart Helm est disponible sur [https://github.com/grafana/helm-charts/tree/main/charts/loki-stack](https://github.com/grafana/helm-charts/tree/main/charts/loki-stack)

## Add Loki repo

Premièrement, ajoutez le repository Grafana à l'aide de la commande suivante

````
$ helm repo add grafana https://grafana.github.io/helm-charts
````

Ensuite, mettez à jour l'ensemble des repos afin que le client Helm connaisse les différentes versions des charts

````
$ helm repo update
````

## Installation

Créez tout d'abord le namespace *loki* dans lequel sera déployée cette stack

````
$ kubectl create ns loki
````

Installez ensuite la stack loki en tant que chart Helm. Utilisez pour cela la commande suivante, celle-ci définit les différents composants qui seront installés:

- *fluent-bit* pour la collecte des logs
- *prometheus* pour la collecte et le stockage de données de monitoring
- *grafana* pour la visualisation des données (à la fois des métriques et des logs)

````
$ helm upgrade --install loki grafana/loki-stack \
  --namespace loki \
  --set fluent-bit.enabled=true \
  --set promtail.enabled=false \
  --set prometheus.enabled=true \
  --set grafana.enabled=true
````

- Get grafana password

````
$ kubectl get secret loki-grafana --namespace=loki -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
````

- Access Grafana

````
$ kubectl port-forward --namespace loki service/loki-grafana 3000:80
````

